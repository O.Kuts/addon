function onGmailMessage(e) {
}

function onGmailCompose(e) {
  const draft = GmailApp.getDrafts()[0]
  const message = draft.getMessage()
  const cc = message.getCc()
  const bcc = message.getBcc()
  const textHtmlContent = '<span style="display: block; font-weight: 500;"> Carbon Copy</span> </br> ';
  var response
  if(cc.length !== 0 || bcc.length !== 0){
    var response = CardService.newUpdateDraftActionResponseBuilder()
        .setUpdateDraftBodyAction(CardService.newUpdateDraftBodyAction()
            .addUpdateContent(textHtmlContent,CardService.ContentType.MUTABLE_HTML)
            .setUpdateType(CardService.UpdateDraftBodyType.IN_PLACE_INSERT))
        .build();
  }else{
    return
  }
  return response;
}

